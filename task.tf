variable "node_count" {
  default = "3"
 }

resource "google_compute_disk" "test-node-1-index-disk-" {
    count   = "${var.node_count}"
    name    = "test-node-1-index-disk-${count.index}-data"
    type    = "pd-standard"
    zone    = "europe-west3-c"
    size    = "20"
}
resource "google_compute_instance" "test-node-" {
    count = "${var.node_count}"
    name = "test-node-${count.index + 1}"
    machine_type = "e2-small"
    zone = "europe-west3-c"

    boot_disk {
    initialize_params {
    image = "debian-cloud/debian-10"
    }
   }
    attached_disk {
        source      = "${element(google_compute_disk.test-node-1-index-disk-.*.self_link, count.index)}"
        device_name = "${element(google_compute_disk.test-node-1-index-disk-.*.name, count.index)}"
   }


    network_interface {
      network = "default"
      access_config {
        // Ephemeral IP
      }

    }
}
